using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private float speed = 2f;
    [SerializeField] private float jumpForce = 15f;
    [SerializeField] private Collider2D feetCollaider;
    [SerializeField] private KeyCode jumpButton = KeyCode.Space;
    [SerializeField] private string groundLayer = "Ground";

    private Rigidbody2D playerRigidbody;
    private Animator playerAnimator;
    private SpriteRenderer playerSpriteRenderer;
    private bool isGrounded;
    

    private void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
        playerSpriteRenderer = GetComponent<SpriteRenderer>();
    }



    private void Update()
    {
        float playerInput = Input.GetAxis("Horizontal");
        Move(playerInput);
        SwitchAnimation(playerInput);
        Flip(playerInput);
        isGrounded = feetCollaider.IsTouchingLayers(LayerMask.GetMask(groundLayer));
        if (Input.GetKeyDown(jumpButton) && isGrounded)
        {
            jump();

        }
        
    } 

    private void Move(float direction)
    {
        playerRigidbody.velocity = new Vector2(direction * speed, playerRigidbody.velocity.y);
    }

    private void jump()
    {
        Vector2 jumpVector = new Vector2(playerRigidbody.velocity.x, jumpForce);
        playerRigidbody.velocity += jumpVector;
    }

    private void SwitchAnimation(float playerInput)
    {
        playerAnimator.SetBool("Run", playerInput != 0);
    }

    private void Flip(float playerInput)
    {
        bool isSpiteFlip = playerSpriteRenderer.flipX;
        if (playerInput <= 0)
        {
            playerSpriteRenderer.flipX = true;
        }
        if (playerInput >= 0)
        {
            playerSpriteRenderer.flipX = false;
        }
    }
}
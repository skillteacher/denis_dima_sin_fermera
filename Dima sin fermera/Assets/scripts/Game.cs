using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Game : MonoBehaviour
{
    public static Game game;
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private TextMeshProUGUI finalCoinCount;
    [SerializeField] private int startLives;
    [SerializeField] private TextMeshProUGUI livesText;
    [SerializeField] private TextMeshProUGUI coinsText;
    [SerializeField] private string firstLevelName = "lvl 1";
    private int lives;
    private int coins;

    private void Awake()
    {
        if (game == null)
        {
            game = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        lives = startLives;
        coins = 0;
        ShowCoins(coins);
        ShowLives(lives);
    }

    public void LoseLive()
    {
        lives--;
        if(lives <=0)
        {
            lives = 0;
            GameOver();
        }
        ShowLives(lives);
    }

    public void AddCoins(int amount)
    {
        coins += amount;
        ShowCoins(coins);
    }

    private void ShowLives(int amount)
    {
        livesText.text = amount.ToString();
    }
    private void ShowCoins(int amount)
    {
        coinsText.text = amount.ToString();
    }

    public void GameOver()
    {
        Time.timeScale = 0f;
        mainMenu.SetActive(true);
        finalCoinCount.text = coins.ToString();
    }

    public void RestartGame()
    {
        lives = startLives;
        coins = 0;
        SceneManager.LoadScene(firstLevelName);
        ShowLives(lives);
        ShowCoins(coins);
        mainMenu.SetActive(false);
        Time.timeScale = 1f;
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
    
    
